class CommentsController < ApplicationController
<<<<<<< HEAD
	http_basic_authenticate_with name: "dhh", password: "secret", only: :destroy

=======
>>>>>>> a98b3d826395d2c01520de359b795fa9b733cd18
  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.create(comment_params)
    redirect_to article_path(@article)
  end

  def destroy
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    @comment.destroy
    redirect_to article_path(@article)
  end

  private
    def comment_params
      params.require(:comment).permit(:commenter, :body)
    end
end